const weatherData = {
    tempUnit: '°C',
    windSpeedUnit: 'm/s',

    days: [
        {day: 'Mon', temp: 22, windDirection: 'north-east', windSpeed: 10, type: 'sunny'},
        {day: 'Tue', temp: 14, windDirection: 'north-west', windSpeed: 14, type: 'rainy'},
        {day: 'Wed', temp: 18, windDirection: 'south-east', windSpeed: 20, type: 'cloudy'},
        {day: 'Thu', temp: 15, windDirection: 'south-west', windSpeed: 25, type: 'thunder'},
        {day: 'Fri', temp: 1, windDirection: 'north', windSpeed: 20, type: 'snow'},
        {day: 'Sat', temp: 13, windDirection: 'south-east', windSpeed: 5, type: 'sunny'},
        {day: 'Sun', temp: 12, windDirection: 'south', windSpeed: 11, type: 'cloudy'}
    ]
};

window.onload = function() {
    initializeDays();
    handleClickItem();
};

function initializeDays() {
    weatherData.days.forEach(function(el) {
        const days = document.getElementById(el.day).getElementsByClassName('day');
        for (var i = 0; i < days.length; i++) {
            days.item(i).textContent = getFullName(el.day);
        }

        const temps = document.getElementById(el.day).getElementsByClassName('temp');
        for (i = 0; i < temps.length; i++) {
            temps.item(i).textContent = convertTemperature(el.temp) + weatherData.tempUnit;
        }
    });
}

function handleClickItem() {
    const weatherItems = document.getElementsByClassName('item');
    for (var i = 0; i < weatherItems.length; i++) {
        weatherItems.item(i).onclick = showDetails;
    }
}

function showDetails() {
    let weatherInfo = weatherData.days.find(weather => weather.day === this.id);

    document.getElementById('info-day').textContent = getFullName(weatherInfo.day);
    document.getElementById('info-temp').textContent = convertTemperature(weatherInfo.temp) + weatherData.tempUnit;
    document.getElementById('info-temp').dataset.temp = weatherInfo.temp;
    document.getElementById('info-wind').className = weatherInfo.windDirection;
    document.getElementById('info-speed').textContent = convertWind(weatherInfo.windSpeed) + weatherData.windSpeedUnit;
    document.getElementById('info-speed').dataset.speed = weatherInfo.windSpeed;
    document.getElementById('info-type').className = weatherInfo.type;

    document.getElementById('widget').classList.remove('hidden');
}

function handleSelectMeasure(radio) {
    weatherData.tempUnit = radio.value;
    initializeDays();
    if (!document.getElementById('widget').classList.contains('hidden')) {
        const weatherInfoTemperature = document.getElementById('info-temp');
        weatherInfoTemperature.textContent = convertTemperature(parseInt(weatherInfoTemperature.dataset.temp)) + weatherData.tempUnit;
    }
}

function handleSelectWind(radio) {
    weatherData.windSpeedUnit = radio.value;
    if (!document.getElementById('widget').classList.contains('hidden')) {
        const weatherInfoSpeed = document.getElementById('info-speed');
        weatherInfoSpeed.textContent = convertWind(parseInt(weatherInfoSpeed.dataset.speed)) + weatherData.windSpeedUnit;
    }
}

function convertTemperature(value) {
    if (weatherData.tempUnit === 'K') {
        return value + 273.15;
    }

    return value;
}

function convertWind(value) {
    if (weatherData.windSpeedUnit === 'km/h') {
        return value * 3.6;
    }

    return value;
}

function getFullName(day) {
    switch (day) {
        case 'Mon':
            return 'Monday';
        case 'Tue':
            return 'Tuesday';
        case 'Wed':
            return 'Wednesday';
        case 'Thu':
            return 'Thursday';
        case 'Fri':
            return 'Friday';
        case 'Sat':
            return 'Saturday';
        case 'Sun':
            return 'Sunday';
        default:
            return 'Unknown';
    }
}

